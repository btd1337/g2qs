export class Answer {
	idQuestion: string;
	idStudent: string;
	private _correctAnswers: number;
	private _wrongAnswers: number;

	constructor(idQuestion: string, idStudent: string) {
		this.idQuestion = idQuestion;
		this.idStudent = idStudent;
		this._correctAnswers = 0;
		this._wrongAnswers = 0;
	}

	addCorrectAnswer(): void {
		this._correctAnswers = this._correctAnswers++;
	}

	get correctAnswers(): number {
		return this._correctAnswers;
	}

	addWrongAnswer(): void {
		this._wrongAnswers = this._wrongAnswers++;
	}

	get wrongAnswers(): number {
		return this._wrongAnswers;
	}

	equals(idStudent: string, idQuestion: string) {
		return idStudent === this.idStudent && idQuestion === this.idQuestion;
	}
}
