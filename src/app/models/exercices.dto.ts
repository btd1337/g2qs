export class ExercicesDto {
	_id: {
		$oid: string;
	};
	titulo: string;
	descricao: string;
	avaliador: string;
	nivelDificuldade: number;
}
