export class UsersDto {
	_id: {
		$oid: string;
	};
	permissions: string[];
	nome: string;
	email: string;
	senha: string;
	nivel: number;
}
