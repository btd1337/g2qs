export class AttemptsDto {
	_id: {
		$oid: string;
	};
	idUsuario: string;
	idExercicio: string;
	resposta: string;
	tempo: number;
	dataInicial: {
		$date: string;
	};
}
