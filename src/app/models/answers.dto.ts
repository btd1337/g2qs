export class AnswersDto {
	_id: {
		$oid: string;
	};
	idUsuario: string;
	idExercicio: string;
	dataInicial: {
		$date: string;
	};
}
