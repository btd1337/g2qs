import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{ path: '', pathMatch: 'full', redirectTo: '/home' },
	{
		path: 'home',
		loadChildren: () =>
			import('./pages/home/home.module').then((m) => m.HomeModule),
	},
	{
		path: 'experiment1',
		loadChildren: () =>
			import('./pages/experiment1/experiment1.module').then(
				(m) => m.Experiment1Module
			),
	},
	{
		path: 'coding-training',
		loadChildren: () =>
			import('./pages/coding-training/coding-training.module').then(
				(m) => m.CodingTrainingModule
			),
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
