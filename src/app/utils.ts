import { ChartData } from './chart-data.interface';

import Player from './glicko-2/player.entity';
import { ChartHorizontalBarData } from './utils/charts/chart-horizontal-bar-data.interface';

export class Utils {
	public static getTeamRatingsSum = (team: Player[]): number => {
		let teamRatingsSum = 0;

		for (let i = 0; i < team.length; i++) {
			teamRatingsSum += team[i].skill;
		}
		return teamRatingsSum;
	};

	public static normalizeStringLength(value: string, length: number): string {
		let formatedValue: string = value;
		let diffLenght = length - formatedValue.length;

		if (diffLenght != 0) {
			formatedValue = formatedValue.padStart(length, '0');
		}
		return formatedValue;
	}

	public static getSkillChartData(players: Player[]) {
		let datas: ChartData[] = new Array<ChartData>();
		datas.push({ name: 'Real', series: [] });
		datas.push({ name: 'Min', series: [] });
		datas.push({ name: 'Max', series: [] });
		datas.push({ name: 'Elo', series: [] });
		let sortedPlayers: Player[] = players;
		sortedPlayers.sort((a, b) => (a.skill > b.skill ? 1 : -1));

		for (let i = 0; i < sortedPlayers.length; i++) {
			datas[0].series.push({
				name: players[i].name[0],
				value: players[i].skill,
			});

			datas[1].series.push({
				name: players[i].name[0],
				value: players[i].minApparentSkill,
			});

			datas[2].series.push({
				name: players[i].name[0],
				value: players[i].maxApparentSkill,
			});

			datas[3].series.push({
				name: players[i].name[0],
				value: players[i].ratingElo,
			});
		}

		return datas;
	}

	public static getBarHorizontalChartData(player: Player) {
		let datas: ChartHorizontalBarData[] = new Array<ChartHorizontalBarData>();
		datas.push({ name: 'Min', value: player.minApparentSkill });
		datas.push({ name: 'Real', value: player.skill });
		datas.push({ name: 'Max', value: player.maxApparentSkill });
		datas.push({ name: 'Elo', value: player.ratingElo });

		return datas;
	}

	public static getRatingVariationChartData(players: Player[]) {
		let datas: ChartData[] = new Array<ChartData>();
		let sortedPlayers: Player[] = players;
		sortedPlayers.sort((a, b) => (a.name > b.name ? 1 : -1));

		for (let i = 0; i < sortedPlayers.length; i++) {
			datas.push({ name: sortedPlayers[i].name[0], series: [] });

			for (let j = 0; j < sortedPlayers[i].ratingHistory.length; j++) {
				datas[i].series.push({
					name: j.toString(),
					value: sortedPlayers[i].ratingHistory[j],
				});
			}
		}

		return datas;
	}
}
