import { Component } from '@angular/core';
import { Location } from '@angular/common';
import Match from 'src/app/glicko-2/match.entity';
import { Utils } from 'src/app/utils';
import { Winner } from 'src/app/glicko-2/winner.enum';
import Player from 'src/app/glicko-2/player.entity';
import {
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from '@angular/forms';
import { ConstantsService } from 'src/app/glicko-2/constants.service';
import { PlayerHandlersService } from 'src/app/glicko-2/player-handlers.service';
import { ChartProperties } from 'src/app/utils/chart-properties.interface';

import * as Elo from 'arpad';
@Component({
	selector: 'app-experiment1',
	templateUrl: './experiment1.component.html',
	styleUrls: ['./experiment1.component.scss'],
})
export class Experiment1Component {
	students: Player[] = new Array<Player>();
	questions: Player[] = new Array<Player>();
	maxSkill: number;
	numAttemps: number = 10;

	differenceBetweenSkills: ChartProperties = {
		legend: true,
		legendTitle: 'Legenda',
		showLabels: true,
		animations: true,
		xAxis: true,
		yAxis: true,
		showYAxisLabel: true,
		showXAxisLabel: true,
		xAxisLabel: 'Questão',
		yAxisLabel: 'Classificação',
		timeline: true,
		data: [],
		view: [900, 500],
		colorScheme: {
			domain: [
				'#24BAE3',
				'#5AA454',
				'#E44D25',
				'#F8CF2A',
				'#9e57bc',
				'#7aa3e5',
				'#f35e44',
				'#a8385d',
				'#aae3f5',
			],
		},
	};

	differenceBetweenSkillsStudents: ChartProperties = {
		legend: true,
		legendTitle: 'Legenda',
		showLabels: true,
		animations: true,
		xAxis: true,
		yAxis: true,
		showYAxisLabel: true,
		showXAxisLabel: true,
		xAxisLabel: 'Estudantes',
		yAxisLabel: 'Classificação',
		timeline: true,
		data: [],
		view: [900, 500],
		colorScheme: {
			domain: [
				'#5AA454',
				'#24BAE3',
				'#E44D25',
				'#F8CF2A',
				'#9e57bc',
				'#7aa3e5',
				'#f35e44',
				'#a8385d',
				'#aae3f5',
			],
		},
	};

	difficultyVariation: ChartProperties = {
		legend: true,
		legendTitle: 'Legenda',
		showLabels: true,
		animations: true,
		xAxis: true,
		yAxis: true,
		showYAxisLabel: true,
		showXAxisLabel: true,
		xAxisLabel: 'Tentativa',
		yAxisLabel: 'Classificação',
		timeline: true,
		data: [],
		view: [900, 500],
		colorScheme: {
			domain: [
				'#5AA454',
				'#24BAE3',
				'#E44D25',
				'#F8CF2A',
				'#9e57bc',
				'#7aa3e5',
				'#f35e44',
				'#a8385d',
				'#aae3f5',
			],
		},
		autoScale: true,
	};

	difficultyVariationStudents: ChartProperties = {
		legend: true,
		legendTitle: 'Legenda',
		showLabels: true,
		animations: true,
		xAxis: true,
		yAxis: true,
		showYAxisLabel: true,
		showXAxisLabel: true,
		xAxisLabel: 'Tentativa',
		yAxisLabel: 'Classificação',
		timeline: true,
		data: [],
		view: [900, 500],
		colorScheme: {
			domain: [
				'#5AA454',
				'#24BAE3',
				'#E44D25',
				'#F8CF2A',
				'#9e57bc',
				'#7aa3e5',
				'#f35e44',
				'#a8385d',
				'#aae3f5',
			],
		},
		autoScale: true,
	};

	validateForm: FormGroup;
	initialFormState;

	utils: Utils;
	isLoadingData: boolean;

	maxMatches: string;
	mediumMatches: string;
	mediumRD: string;

	studentsStats: {
		maxMatches: string;
		mediumMatches: string;
		mediumRD: string;
	};

	elo: Elo;

	constructor(
		private _location: Location,
		private fb: FormBuilder,
		private constantsService: ConstantsService,
		private playerHandlers: PlayerHandlersService
	) {
		Object.assign(this);
		this.maxSkill = this.constantsService.MAX_SKILL;
		this.isLoadingData = false;
		this.setForm();

		this.elo = new Elo();
	}

	onSelect(data): void {
		// console.log('Item clicked', JSON.parse(JSON.stringify(data)));
	}

	onActivate(data): void {
		// console.log('Activate', JSON.parse(JSON.stringify(data)));
	}

	onDeactivate(data): void {
		// console.log('Deactivate', JSON.parse(JSON.stringify(data)));
	}

	onBack(): void {
		this._location.back();
	}

	createPlayer(name: string, skill: number): Player {
		return Player.createPlayerFactory(name, skill, {
			defaultRating: this.constantsService.DEFAULT_RATING,
			defaultRatingDeviation: this.constantsService.DEFAULT_RD,
			defaultVolatility: this.constantsService.DEFAULT_VOLATILITY,
			tau: this.constantsService.DEFAULT_TAU,
		});
	}

	recordMatch(match: Match) {
		let newRatingA: number;
		let newRatingB: number;
		let teamASumRatings =
			match.aTeam.length == 1
				? match.aTeam[0].skill
				: Utils.getTeamRatingsSum(match.aTeam);
		let teamBSumRatings =
			match.bTeam.length == 1
				? match.bTeam[0].skill
				: Utils.getTeamRatingsSum(match.bTeam);

		const numberDraw = this.playerHandlers.randomInt(
			0,
			teamASumRatings + teamBSumRatings
		);
		const winner: Winner =
			numberDraw < teamASumRatings ? Winner.PLAYER_A : Winner.PLAYER_B;

		switch (winner) {
			case Winner.PLAYER_A: {
				// GLICKO2
				match.reportTeamAWon();

				// ELO
				newRatingA = this.elo.newRatingIfWon(
					match.aTeam[0].ratingElo,
					match.bTeam[0].ratingElo
				);
				newRatingB = this.elo.newRatingIfLost(
					match.bTeam[0].ratingElo,
					match.aTeam[0].ratingElo
				);
				match.aTeam[0].ratingElo = newRatingA;
				match.bTeam[0].ratingElo = newRatingB;
				break;
			}
			case Winner.PLAYER_B: {
				// GLICKO2
				match.reportTeamBWon();

				// ELO
				newRatingB = this.elo.newRatingIfWon(
					match.bTeam[0].ratingElo,
					match.aTeam[0].ratingElo
				);
				newRatingA = this.elo.newRatingIfLost(
					match.aTeam[0].ratingElo,
					match.bTeam[0].ratingElo
				);
				match.bTeam[0].ratingElo = newRatingB;
				match.aTeam[0].ratingElo = newRatingA;
				break;
			}
			default: {
				console.log('Invalid Result');
			}
		}
	}

	setForm() {
		this.validateForm = this.fb.group({
			defaultRD: [
				this.constantsService.DEFAULT_RD,
				[Validators.required, Validators.min(1), Validators.max(3000)],
			],
			defaultVolatility: [
				this.constantsService.DEFAULT_VOLATILITY,
				[Validators.required, Validators.min(0), Validators.max(5)],
			],
			defaultTau: [
				this.constantsService.DEFAULT_TAU,
				[Validators.required, Validators.min(0), Validators.max(2)],
			],
			seed: [this.constantsService.getSeed(), [Validators.required]],
		});

		this.initialFormState = this.validateForm.value;
	}

	setStats(): void {
		let stats = this.playerHandlers.print_players(this.questions);
		this.maxMatches = stats.maxMatches;
		this.mediumMatches = stats.mediumMatches;
		this.mediumRD = stats.mediumRD;

		this.studentsStats = this.playerHandlers.print_players(this.students);
	}

	resetStats(): void {
		this.maxMatches = '0';
		this.mediumMatches = '0';
		this.mediumRD = '0';

		this.studentsStats.maxMatches = '0';
		this.studentsStats.mediumMatches = '0';
		this.studentsStats.mediumRD = '0';
	}

	resetForm(e: MouseEvent): void {
		e.preventDefault();
		this.validateForm.reset(this.initialFormState);
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsPristine();
			this.validateForm.controls[key].updateValueAndValidity();
		}
		this.students = [];
		this.questions = [];
		this.differenceBetweenSkills.data = [];
		this.differenceBetweenSkillsStudents.data = [];
		this.difficultyVariation.data = [];
		this.difficultyVariationStudents.data = [];
		this.constantsService.updateRandom();
		this.isLoadingData = false;
	}

	confirmValidator = (control: FormControl): { [s: string]: boolean } => {
		if (!control.value) {
			return { error: true, required: true };
		} else if (control.value !== this.validateForm.controls.password.value) {
			return { confirm: true, error: true };
		}
		return {};
	};

	createFakeStudents() {
		for (let i: number = 1; i <= this.constantsService.NUM_STUDENTS; i++) {
			let id: string = Utils.normalizeStringLength(
				i.toString(),
				this.constantsService.NUM_STUDENTS_LENGTH
			);
			this.students.push(
				this.createPlayer(
					`Estudante ${id}`,
					this.playerHandlers.random_normal()
				)
			);
		}
	}

	createFakeQuestions() {
		for (let i: number = 1; i <= this.constantsService.NUM_QUESTIONS; i++) {
			let id: string = Utils.normalizeStringLength(
				i.toString(),
				this.constantsService.NUM_QUESTIONS_LENGTH
			);
			this.questions.push(
				this.createPlayer(`Questão ${id}`, this.playerHandlers.random_normal())
			);
		}
	}

	run() {
		this.isLoadingData = true;
		this.students = [];
		this.questions = [];
		this.constantsService.DEFAULT_RD = this.validateForm.get('defaultRD').value;
		this.constantsService.DEFAULT_VOLATILITY = this.validateForm.get(
			'defaultVolatility'
		).value;
		this.constantsService.DEFAULT_TAU = this.validateForm.get(
			'defaultTau'
		).value;
		this.constantsService.updateSeed(this.validateForm.get('seed').value);
		this.constantsService.updateRandom();

		this.differenceBetweenSkills.data = [];

		this.createFakeStudents();

		this.questions.push(
			this.createPlayer(`Questão 1`, this.constantsService.MAX_SKILL * (1 / 3))
		);
		this.questions.push(
			this.createPlayer(`Questão 2`, this.constantsService.MAX_SKILL * (1 / 2))
		);
		this.questions.push(
			this.createPlayer(`Questão 3`, this.constantsService.MAX_SKILL * (2 / 3))
		);

		// Todos os estudantes realizam X questões

		for (let i = 0; i < this.numAttemps; i++) {
			for (let j = 0; j < this.questions.length; j++) {
				for (let k = 0; k < this.students.length; k++) {
					this.recordMatch(new Match(this.students[k], this.questions[j]));
					this.students[k].updateRating();

					// necessário para impedir o glitch que faz inesperadamente (ainda não identificado a causa) o RD assumir valores altos e aleatórios
					if (k % this.constantsService.ROUNDS_TO_UPDATE == 0) {
						this.questions[j].updateRating();
					}
					console.log(
						`T ${i + 1} | Q ${j + 1}: ${this.questions[j].rating} | E ${
							k + 1
						}: ${this.students[k].rating}`
					);
				}
			}
		}

		// this.differenceBetweenSkills.data = Utils.getSkillChartData(this.questions);
		this.differenceBetweenSkills.data = Utils.getBarHorizontalChartData(
			this.questions[0]
		);

		this.differenceBetweenSkillsStudents.data = Utils.getSkillChartData(
			this.students
		);

		this.difficultyVariation.data = Utils.getRatingVariationChartData(
			this.questions
		);

		let sortedStudents: Player[] = this.students;
		sortedStudents.sort((a, b) => (a.rating > b.rating ? 1 : -1));

		let auxStudents: Player[] = [
			sortedStudents[15],
			sortedStudents[250],
			sortedStudents[485],
		];

		this.difficultyVariationStudents.data = Utils.getRatingVariationChartData(
			auxStudents
		);

		this.setStats();

		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsDirty();
			this.validateForm.controls[key].updateValueAndValidity();
		}
		this.isLoadingData = false;
	}
}
