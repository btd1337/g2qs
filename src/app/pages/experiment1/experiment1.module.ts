import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';

import { Experiment1RoutingModule } from './experiment1-routing.module';

import { Experiment1Component } from './experiment1.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTypographyModule } from 'ng-zorro-antd/typography';

@NgModule({
	imports: [
		Experiment1RoutingModule,
		NgxChartsModule,
		NzPageHeaderModule,
		NzButtonModule,
		NzFormModule,
		NzIconModule,
		NzInputModule,
		NzInputNumberModule,
		NzSpinModule,
		NzTypographyModule,
		FormsModule,
		ReactiveFormsModule,
	],
	declarations: [Experiment1Component],
	exports: [Experiment1Component],
})
export class Experiment1Module {}
