import { Component } from '@angular/core';
import { Location } from '@angular/common';
import Match from 'src/app/glicko-2/match.entity';
import { Utils } from 'src/app/utils';
import { Winner } from 'src/app/glicko-2/winner.enum';
import Player from 'src/app/glicko-2/player.entity';
import {
	FormBuilder,
	FormControl,
	FormGroup,
	Validators,
} from '@angular/forms';
import { ConstantsService } from 'src/app/glicko-2/constants.service';
import { PlayerHandlersService } from 'src/app/glicko-2/player-handlers.service';
import { ChartProperties } from 'src/app/utils/chart-properties.interface';

import * as Elo from 'arpad';
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
	students: Player[] = new Array<Player>();
	questions: Player[] = new Array<Player>();
	maxSkill: number;

	differenceBetweenSkills: ChartProperties = {
		legend: true,
		legendTitle: 'Legenda',
		showLabels: true,
		animations: true,
		xAxis: true,
		yAxis: true,
		showYAxisLabel: true,
		showXAxisLabel: true,
		xAxisLabel: 'Questão',
		yAxisLabel: 'Classificação',
		timeline: true,
		data: [],
		view: [900, 500],
		colorScheme: {
			domain: [
				'#5AA454',
				'#24BAE3',
				'#E44D25',
				'#F8CF2A',
				'#7aa3e5',
				'#a8385d',
				'#aae3f5',
			],
		},
	};

	difficultyVariation: ChartProperties = {
		legend: true,
		legendTitle: 'Legenda',
		showLabels: true,
		animations: true,
		xAxis: true,
		yAxis: true,
		showYAxisLabel: true,
		showXAxisLabel: true,
		xAxisLabel: 'Resposta',
		yAxisLabel: 'Classificação',
		timeline: true,
		data: [],
		view: [900, 500],
		colorScheme: {
			domain: [
				'#5AA454',
				'#24BAE3',
				'#E44D25',
				'#7aa3e5',
				'#a8385d',
				'#aae3f5',
			],
		},
	};

	validateForm: FormGroup;
	initialFormState;

	utils: Utils;
	isLoadingData: boolean;

	maxMatches: string;
	mediumMatches: string;
	mediumRD: string;

	elo: Elo;

	constructor(
		private _location: Location,
		private fb: FormBuilder,
		private constantsService: ConstantsService,
		private playerHandlers: PlayerHandlersService
	) {
		Object.assign(this);
		this.maxSkill = this.constantsService.MAX_SKILL;
		this.isLoadingData = false;
		this.setForm();

		this.elo = new Elo();
	}

	onSelect(data): void {
		// console.log('Item clicked', JSON.parse(JSON.stringify(data)));
	}

	onActivate(data): void {
		// console.log('Activate', JSON.parse(JSON.stringify(data)));
	}

	onDeactivate(data): void {
		// console.log('Deactivate', JSON.parse(JSON.stringify(data)));
	}

	onBack(): void {
		this._location.back();
	}

	createPlayer(name: string, skill: number): Player {
		return Player.createPlayerFactory(name, skill, {
			defaultRating: this.constantsService.DEFAULT_RATING,
			defaultRatingDeviation: this.constantsService.DEFAULT_RD,
			defaultVolatility: this.constantsService.DEFAULT_VOLATILITY,
			tau: this.constantsService.DEFAULT_TAU,
		});
	}

	recordMatch(match: Match) {
		let newRatingA: number;
		let newRatingB: number;
		let teamASumRatings =
			match.aTeam.length == 1
				? match.aTeam[0].skill
				: Utils.getTeamRatingsSum(match.aTeam);
		let teamBSumRatings =
			match.bTeam.length == 1
				? match.bTeam[0].skill
				: Utils.getTeamRatingsSum(match.bTeam);

		const numberDraw = this.playerHandlers.randomInt(
			0,
			teamASumRatings + teamBSumRatings
		);
		const winner: Winner =
			numberDraw < teamASumRatings ? Winner.PLAYER_A : Winner.PLAYER_B;

		switch (winner) {
			case Winner.PLAYER_A: {
				match.reportTeamAWon();
				newRatingA = this.elo.newRatingIfWon(
					match.aTeam[0].ratingElo,
					match.bTeam[0].ratingElo
				);
				newRatingB = this.elo.newRatingIfLost(
					match.bTeam[0].ratingElo,
					match.aTeam[0].ratingElo
				);
				match.aTeam[0].ratingElo = newRatingA;
				match.bTeam[0].ratingElo = newRatingB;
				break;
			}
			case Winner.PLAYER_B: {
				match.reportTeamBWon();
				newRatingB = this.elo.newRatingIfWon(
					match.bTeam[0].ratingElo,
					match.aTeam[0].ratingElo
				);
				newRatingA = this.elo.newRatingIfLost(
					match.aTeam[0].ratingElo,
					match.bTeam[0].ratingElo
				);
				match.bTeam[0].ratingElo = newRatingB;
				match.aTeam[0].ratingElo = newRatingA;
				break;
			}
			default: {
				console.log('Invalid Result');
			}
		}
	}

	run() {
		this.isLoadingData = true;
		this.students = [];
		this.questions = [];
		this.constantsService.DEFAULT_RD = this.validateForm.get('defaultRD').value;
		this.constantsService.DEFAULT_VOLATILITY = this.validateForm.get(
			'defaultVolatility'
		).value;
		this.constantsService.DEFAULT_TAU = this.validateForm.get(
			'defaultTau'
		).value;
		this.constantsService.updateSeed(this.validateForm.get('seed').value);
		this.constantsService.updateRandom();

		this.differenceBetweenSkills.data = [];

		for (let i: number = 1; i <= this.constantsService.NUM_STUDENTS; i++) {
			let id: string = Utils.normalizeStringLength(
				i.toString(),
				this.constantsService.NUM_STUDENTS_LENGTH
			);
			this.students.push(
				this.createPlayer(`S ${id}`, this.playerHandlers.random_normal())
			);
		}

		for (let i: number = 1; i <= this.constantsService.NUM_QUESTIONS; i++) {
			let id: string = Utils.normalizeStringLength(
				i.toString(),
				this.constantsService.NUM_QUESTIONS_LENGTH
			);
			this.questions.push(
				this.createPlayer(`Q ${id}`, this.playerHandlers.random_normal())
			);
		}

		let question;
		let student;

		// Todos os estudantes realizam X questões

		/*
    for (let i = 0; i < students.length; i++) {
      for (let j = 0; j < 20; j++) {
        question = Utils.randomInt(0, NUM_QUESTIONS - 1);
        recordMatch(new Match(students[i], questions[question]));
        students[i].updateRating();
        questions[question].updateRating();
      }
    }
    */

		// Todas as questões são resolvidas até chegarem em uma margem de erro específica

		for (let i = 0; i < this.questions.length; i++) {
			do {
				student = this.playerHandlers.randomInt(
					0,
					this.constantsService.NUM_STUDENTS - 1
				);
				this.recordMatch(new Match(this.students[student], this.questions[i]));
				this.questions[i].updateRating();
				this.students[student].updateRating();
				/* console.log(
					`${this.questions[i].toStringFormatted(
						this.constantsService.RATING_LENGTH,
						this.constantsService.RD_LENGTH
					)}`
				); */
			} while (
				!this.questions[i].checkSkillConfiability(
					this.constantsService.MAX_SKILL,
					this.constantsService.UNCERTAINTY_RATE
				)
			);
		}

		// console.log('\n\n--- ESTUDANTES ---\n\n');
		// Utils.print_players(students);

		// console.log('\n\n--- QUESTÕES ---\n\n');
		// this.playerHandlers.print_players(this.questions);
		this.differenceBetweenSkills.data = Utils.getSkillChartData(this.questions);

		this.difficultyVariation.data = Utils.getRatingVariationChartData(
			this.questions
		);

		this.setStats();

		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsDirty();
			this.validateForm.controls[key].updateValueAndValidity();
		}
		this.isLoadingData = false;
	}

	setForm() {
		this.validateForm = this.fb.group({
			defaultRD: [
				this.constantsService.DEFAULT_RD,
				[Validators.required, Validators.min(1), Validators.max(3000)],
			],
			defaultVolatility: [
				this.constantsService.DEFAULT_VOLATILITY,
				[Validators.required, Validators.min(0), Validators.max(5)],
			],
			defaultTau: [
				this.constantsService.DEFAULT_TAU,
				[Validators.required, Validators.min(0), Validators.max(2)],
			],
			seed: [this.constantsService.getSeed(), [Validators.required]],
		});

		this.initialFormState = this.validateForm.value;
	}

	setStats(): void {
		let stats = this.playerHandlers.print_players(this.questions);
		this.maxMatches = stats.maxMatches;
		this.mediumMatches = stats.mediumMatches;
		this.mediumRD = stats.mediumRD;
	}

	resetStats(): void {
		this.maxMatches = '0';
		this.mediumMatches = '0';
		this.mediumRD = '0';
	}

	resetForm(e: MouseEvent): void {
		e.preventDefault();
		this.validateForm.reset(this.initialFormState);
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsPristine();
			this.validateForm.controls[key].updateValueAndValidity();
		}
		this.students = [];
		this.questions = [];
		this.differenceBetweenSkills.data = [];
		this.constantsService.updateRandom();
		this.isLoadingData = false;
	}

	confirmValidator = (control: FormControl): { [s: string]: boolean } => {
		if (!control.value) {
			return { error: true, required: true };
		} else if (control.value !== this.validateForm.controls.password.value) {
			return { confirm: true, error: true };
		}
		return {};
	};
}
