import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {
	FormGroup,
	FormBuilder,
	Validators,
	FormControl,
} from '@angular/forms';
import { ConstantsService } from 'src/app/glicko-2/constants.service';
import Match from 'src/app/glicko-2/match.entity';
import { PlayerHandlersService } from 'src/app/glicko-2/player-handlers.service';
import Player from 'src/app/glicko-2/player.entity';
import { Winner } from 'src/app/glicko-2/winner.enum';
import { Utils } from 'src/app/utils';
import { ChartProperties } from 'src/app/utils/chart-properties.interface';

import { AnswersDto } from 'src/app/models/answers.dto';
import { AttemptsDto } from 'src/app/models/attempts.dto';
import { UsersDto } from 'src/app/models/users.dto';
import { ExercicesDto } from 'src/app/models/exercices.dto';

import answersJson from '../../../assets/data/answers.json';
import attemptsJson from '../../../assets/data/attempts.json';
import exercicesJson from '../../../assets/data/exercices.json';
import usersJson from '../../../assets/data/users.json';
import { Answer } from 'src/app/models/answer.entity';

@Component({
	selector: 'app-coding-training',
	templateUrl: './coding-training.component.html',
	styleUrls: ['./coding-training.component.scss'],
})
export class CodingTrainingComponent implements OnInit {
	students: Player[] = new Array<Player>();
	initStateStudents: Player[] = new Array<Player>();
	questions: Player[] = new Array<Player>();
	initStateQuestions: Player[] = new Array<Player>();
	matches: Answer[] = new Array<Answer>();

	maxSkill: number;
	answersDto: AnswersDto[];
	attemptsDto: AttemptsDto[];
	exercicesDto: ExercicesDto[];
	usersDto: UsersDto[];

	differenceBetweenSkills: ChartProperties = {
		legend: true,
		legendTitle: 'Legenda',
		showLabels: true,
		animations: true,
		xAxis: true,
		yAxis: true,
		showYAxisLabel: true,
		showXAxisLabel: true,
		xAxisLabel: 'Questão',
		yAxisLabel: 'Classificação',
		timeline: true,
		data: [],
		view: [900, 500],
		colorScheme: {
			domain: [
				'#5AA454',
				'#24BAE3',
				'#E44D25',
				'#7aa3e5',
				'#a8385d',
				'#aae3f5',
			],
		},
	};

	difficultyVariation: ChartProperties = {
		legend: true,
		legendTitle: 'Legenda',
		showLabels: true,
		animations: true,
		xAxis: true,
		yAxis: true,
		showYAxisLabel: true,
		showXAxisLabel: true,
		xAxisLabel: 'Resposta',
		yAxisLabel: 'Classificação',
		timeline: true,
		data: [],
		view: [900, 500],
		colorScheme: {
			domain: [
				'#5AA454',
				'#24BAE3',
				'#E44D25',
				'#7aa3e5',
				'#a8385d',
				'#aae3f5',
			],
		},
	};

	validateForm: FormGroup;
	initialFormState;

	utils: Utils;
	isLoadingData: boolean;

	maxMatches: string;
	mediumMatches: string;
	mediumRD: string;

	constructor(
		private _location: Location,
		private fb: FormBuilder,
		private constantsService: ConstantsService,
		private playerHandlers: PlayerHandlersService
	) {
		Object.assign(this);
		this.maxSkill = this.constantsService.MAX_SKILL;
		this.isLoadingData = false;
		this.setForm();
	}

	ngOnInit() {
		this.answersDto = answersJson;
		this.attemptsDto = attemptsJson;
		this.exercicesDto = exercicesJson;
		this.usersDto = usersJson;

		this.createStudents();
		this.createQuestions();
		this.recordAnswers();
	}

	onSelect(data): void {
		// console.log('Item clicked', JSON.parse(JSON.stringify(data)));
	}

	onActivate(data): void {
		// console.log('Activate', JSON.parse(JSON.stringify(data)));
	}

	onDeactivate(data): void {
		// console.log('Deactivate', JSON.parse(JSON.stringify(data)));
	}

	onBack(): void {
		this._location.back();
	}

	createPlayer(name: string, skill: number, id?: string): Player {
		return Player.createPlayerFactory(
			name,
			skill,
			{
				defaultRating: this.constantsService.DEFAULT_RATING,
				defaultRatingDeviation: this.constantsService.DEFAULT_RD,
				defaultVolatility: this.constantsService.DEFAULT_VOLATILITY,
				tau: this.constantsService.DEFAULT_TAU,
			},
			id
		);
	}

	recordMatch(match: Match) {
		let teamASumRatings =
			match.aTeam.length == 1
				? match.aTeam[0].skill
				: Utils.getTeamRatingsSum(match.aTeam);
		let teamBSumRatings =
			match.bTeam.length == 1
				? match.bTeam[0].skill
				: Utils.getTeamRatingsSum(match.bTeam);

		const numberDraw = this.playerHandlers.randomInt(
			0,
			teamASumRatings + teamBSumRatings
		);
		const winner: Winner =
			numberDraw < teamASumRatings ? Winner.PLAYER_A : Winner.PLAYER_B;

		switch (winner) {
			case Winner.PLAYER_A: {
				match.reportTeamAWon();
				break;
			}
			case Winner.PLAYER_B: {
				match.reportTeamBWon();
				break;
			}
			default: {
				console.log('Invalid Result');
			}
		}
	}

	run() {
		this.isLoadingData = true;
		this.students = Object.assign([], this.initStateStudents);
		this.questions = Object.assign([], this.initStateQuestions);
		this.constantsService.DEFAULT_RD = this.validateForm.get('defaultRD').value;
		this.constantsService.DEFAULT_VOLATILITY = this.validateForm.get(
			'defaultVolatility'
		).value;
		this.constantsService.DEFAULT_TAU = this.validateForm.get(
			'defaultTau'
		).value;
		this.constantsService.updateSeed(this.validateForm.get('seed').value);
		this.constantsService.updateRandom();

		this.differenceBetweenSkills.data = [];

		let question;
		let student;

		// Todos os estudantes realizam X questões

		/*
    for (let i = 0; i < students.length; i++) {
      for (let j = 0; j < 20; j++) {
        question = Utils.randomInt(0, NUM_QUESTIONS - 1);
        recordMatch(new Match(students[i], questions[question]));
        students[i].updateRating();
        questions[question].updateRating();
      }
    }
    */

		// Todas as questões são resolvidas até chegarem em uma margem de erro específica

		for (let i = 0; i < this.questions.length; i++) {
			do {
				student = this.playerHandlers.randomInt(0, this.students.length - 1);
				this.recordMatch(new Match(this.students[student], this.questions[i]));
				this.questions[i].updateRating();
				this.students[student].updateRating();
				/* console.log(
					`${this.questions[i].toStringFormatted(
						this.constantsService.RATING_LENGTH,
						this.constantsService.RD_LENGTH
					)}`
				); */
			} while (
				!this.questions[i].checkSkillConfiability(
					this.constantsService.MAX_SKILL,
					this.constantsService.UNCERTAINTY_RATE
				)
			);
		}

		// console.log('\n\n--- ESTUDANTES ---\n\n');
		// Utils.print_players(students);

		// console.log('\n\n--- QUESTÕES ---\n\n');
		// this.playerHandlers.print_players(this.questions);
		this.differenceBetweenSkills.data = Utils.getSkillChartData(this.questions);

		this.difficultyVariation.data = Utils.getRatingVariationChartData(
			this.questions
		);

		this.setStats();

		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsDirty();
			this.validateForm.controls[key].updateValueAndValidity();
		}
		this.isLoadingData = false;
	}

	setForm() {
		this.validateForm = this.fb.group({
			defaultRD: [
				this.constantsService.DEFAULT_RD,
				[Validators.required, Validators.min(1), Validators.max(3000)],
			],
			defaultVolatility: [
				this.constantsService.DEFAULT_VOLATILITY,
				[Validators.required, Validators.min(0), Validators.max(5)],
			],
			defaultTau: [
				this.constantsService.DEFAULT_TAU,
				[Validators.required, Validators.min(0), Validators.max(2)],
			],
			seed: [this.constantsService.getSeed(), [Validators.required]],
		});

		this.initialFormState = this.validateForm.value;
	}

	setStats(): void {
		let stats = this.playerHandlers.print_players(this.questions);
		this.maxMatches = stats.maxMatches;
		this.mediumMatches = stats.mediumMatches;
		this.mediumRD = stats.mediumRD;
	}

	resetStats(): void {
		this.maxMatches = '0';
		this.mediumMatches = '0';
		this.mediumRD = '0';
	}

	resetForm(e: MouseEvent): void {
		e.preventDefault();
		this.validateForm.reset(this.initialFormState);
		for (const key in this.validateForm.controls) {
			this.validateForm.controls[key].markAsPristine();
			this.validateForm.controls[key].updateValueAndValidity();
		}
		this.students = [];
		this.questions = [];
		this.differenceBetweenSkills.data = [];
		this.constantsService.updateRandom();
		this.isLoadingData = false;
	}

	confirmValidator = (control: FormControl): { [s: string]: boolean } => {
		if (!control.value) {
			return { error: true, required: true };
		} else if (control.value !== this.validateForm.controls.password.value) {
			return { confirm: true, error: true };
		}
		return {};
	};

	createStudents() {
		for (let i = 0; i < this.usersDto.length; i++) {
			this.students.push(
				this.createPlayer(
					this.usersDto[i].nome,
					this.playerHandlers.random_normal(),
					this.usersDto[i]._id.$oid
				)
			);
		}

		this.initStateStudents = Object.assign([], this.students);
	}

	createQuestions() {
		for (let i = 0; i < this.exercicesDto.length; i++) {
			this.questions.push(
				this.createPlayer(
					this.exercicesDto[i].titulo,
					this.playerHandlers.random_normal(),
					this.exercicesDto[i]._id.$oid
				)
			);
		}

		this.initStateQuestions = Object.assign([], this.questions);
	}

	recordAnswers() {
		let answerFound: Answer;
		this.answersDto.forEach((answer) => {
			answerFound = this.matches.find(
				(element) =>
					element.idQuestion === answer.idExercicio &&
					element.idStudent === answer.idUsuario
			);
			if (answerFound) {
				if (answerFound.correctAnswers >= 1) {
					answerFound.addWrongAnswer();
				} else {
					answerFound.addCorrectAnswer();
				}
			} else {
				this.matches.push(new Answer(answer.idExercicio, answer.idUsuario));
			}
		});
	}
}
