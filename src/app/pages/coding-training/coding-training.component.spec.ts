import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodingTrainingComponent } from './coding-training.component';

describe('CodingTrainingComponent', () => {
  let component: CodingTrainingComponent;
  let fixture: ComponentFixture<CodingTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CodingTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodingTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
