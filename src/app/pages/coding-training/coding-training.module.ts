import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CodingTrainingComponent } from './coding-training.component';
import { CodingTrainingRoutingModule } from './coding-training-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTypographyModule } from 'ng-zorro-antd/typography';

@NgModule({
	declarations: [CodingTrainingComponent],
	exports: [CodingTrainingComponent],
	imports: [
		CodingTrainingRoutingModule,
		NgxChartsModule,
		NzPageHeaderModule,
		NzButtonModule,
		NzFormModule,
		NzIconModule,
		NzInputModule,
		NzInputNumberModule,
		NzSpinModule,
		NzTypographyModule,
		FormsModule,
		ReactiveFormsModule,
	],
})
export class CodingTrainingModule {}
