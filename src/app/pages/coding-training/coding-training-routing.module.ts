import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CodingTrainingComponent } from './coding-training.component';

const routes: Routes = [{ path: '', component: CodingTrainingComponent }];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class CodingTrainingRoutingModule {}
