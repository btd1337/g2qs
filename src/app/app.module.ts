import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { IconsProviderModule } from './icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { ConstantsService } from './glicko-2/constants.service';
import { PlayerHandlersService } from './glicko-2/player-handlers.service';

registerLocaleData(en);

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		NgxChartsModule,
		IconsProviderModule,
		NzLayoutModule,
		NzMenuModule,
		FormsModule,
		HttpClientModule,
		BrowserAnimationsModule,
	],
	providers: [
		{ provide: NZ_I18N, useValue: en_US },
		ConstantsService,
		PlayerHandlersService,
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
