import { SerieData } from './serie-data.interface';

export interface ChartData {
	name: string;
	series: SerieData[];
}
