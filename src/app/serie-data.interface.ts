export interface SerieData {
	name: string;
	value: number;
}
