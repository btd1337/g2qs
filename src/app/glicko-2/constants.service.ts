import { Injectable } from '@angular/core';
import Chance from 'chance';

@Injectable()
export class ConstantsService {
	// Player Params
	readonly MAX_SKILL: number = 3000;
	readonly MIN_SKILL: number = 1;
	readonly DEFAULT_RATING: number = this.MAX_SKILL / 2;
	DEFAULT_RD: number = 150;
	DEFAULT_VOLATILITY: number = 0.08;
	DEFAULT_TAU: number = 0.4;

	// Instance
	readonly NUM_STUDENTS: number = 500;
	readonly NUM_STUDENTS_LENGTH: number = this.NUM_STUDENTS.toString().length;
	readonly NUM_QUESTIONS: number = 50;
	readonly NUM_QUESTIONS_LENGTH: number = this.NUM_QUESTIONS.toString().length;

	// Settings
	readonly ROUNDS_TO_UPDATE: number = 10;
	readonly RATING_LENGTH: number = this.DEFAULT_RATING.toString().length + 3; // value + '.XX'
	readonly RD_LENGTH: number = this.DEFAULT_RD.toString().length + 3;
	readonly UNCERTAINTY_RATE: number = 0.1;
	readonly DEFAULT_PLAYER_DEVIATION = 300;
	private SEED = 100;
	RANDOM: Chance.Chance;

	constructor() {
		this.RANDOM = new Chance(this.SEED);
	}

	getSeed() {
		return this.SEED;
	}

	updateSeed(seed: any): void {
		this.SEED = seed;
		this.RANDOM = new Chance(seed);
	}

	updateRandom(): void {
		this.RANDOM = new Chance(this.SEED);
	}
}
