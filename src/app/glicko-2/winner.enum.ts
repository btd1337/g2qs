export enum Winner {
	PLAYER_A,
	PLAYER_B,
	TIE,
}
