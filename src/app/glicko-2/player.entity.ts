import { Utils } from '../utils';
import { Outcome } from './outcome.enum';

export interface IPlayerOpts {
	defaultRating: number;
	opponentRatingDeviations?: number[];
	opponentRatings?: number[];
	outcomes?: Outcome[];
	rating: number;
	ratingDeviation: number;
	tau: number;
	volatility: number;
}

export default class Player {
	private static readonly scalingFactor = 173.7178;

	private readonly tau: number;
	private readonly defaultRating: number;

	private _opponentRatingDeviations: number[] = [];
	private _opponentRatings: number[] = [];
	private _outcomes: Outcome[] = [];
	private _rating: number;
	private _ratingDeviation: number;
	private _volatility: number;

	private _id: string;
	private _name: string;
	private _skill: number;
	private _wins: number;
	private _loses: number;
	private _ties: number;
	private _numMatches: number;

	private _ratingHistory: number[];

	private _ratingElo: number;

	constructor(
		name: string,
		skill: number,
		{
			defaultRating,
			opponentRatingDeviations,
			opponentRatings,
			outcomes,
			rating,
			ratingDeviation,
			tau,
			volatility,
		}: IPlayerOpts
	) {
		this.tau = tau;
		this.defaultRating = defaultRating;
		this._rating = (rating - defaultRating) / Player.scalingFactor;
		this._ratingDeviation = ratingDeviation / Player.scalingFactor;
		this._volatility = volatility;

		this._name = name;
		this._skill = skill;
		this._wins = 0;
		this._loses = 0;
		this._ties = 0;
		this._numMatches = 0;

		if (
			Array.isArray(opponentRatingDeviations) ||
			Array.isArray(opponentRatings) ||
			Array.isArray(outcomes)
		) {
			if (
				(outcomes || []).length !== (opponentRatings || []).length ||
				(outcomes || []).length !== (opponentRatingDeviations || []).length
			) {
				throw new Error(
					'opponentRatingDeviations, opponentRatings, outcomes must be of equal size'
				);
			}
			this._opponentRatingDeviations = opponentRatingDeviations
				? opponentRatingDeviations
				: [];
			this._opponentRatings = opponentRatings ? opponentRatings : [];
			this._outcomes = outcomes ? outcomes : [];
		}

		// init history
		this._ratingHistory = new Array<number>();
		this._ratingHistory.push(this.rating);

		// ELO
		this._ratingElo = defaultRating;
	}

	static compositePlayer(players: Player[]) {
		if (players.length === 0) {
			throw new Error('Cannot create a composite of 0 players');
		}
		const [refPlayer] = players;
		let rating = 0;
		let ratingDeviation = 0;
		players.forEach((p, i) => {
			if (!refPlayer.isCompatiblePlayer(p)) {
				throw new Error('All players must have equal tau and defaultRating');
			}
			// Cumulative moving average
			rating = rating + (p.rating - rating) / (i + 1);
			ratingDeviation =
				ratingDeviation + (p.ratingDeviation - ratingDeviation) / (i + 1);
		});
		return new Player(refPlayer._name, refPlayer._skill, {
			defaultRating: refPlayer.defaultRating,
			rating,
			ratingDeviation,
			tau: refPlayer.tau,
			// This is a pseudo player whose resulting volatility is irrelevant
			volatility: refPlayer.volatility,
		});
	}

	get id(): string {
		return this._id;
	}

	get name() {
		return [this._name];
	}

	get skill() {
		return this._skill;
	}

	get opponentRatingDeviations() {
		return [...this._opponentRatingDeviations];
	}

	get opponentRatings() {
		return [...this._opponentRatings];
	}

	get outcomes() {
		return [...this._outcomes];
	}

	get rating() {
		return this._rating * Player.scalingFactor + this.defaultRating;
	}

	get ratingElo() {
		return this._ratingElo;
	}

	get ratingDeviation() {
		return this._ratingDeviation * Player.scalingFactor;
	}

	get volatility() {
		return this._volatility;
	}

	get minApparentSkill(): number {
		return this.rating - 2 * this.ratingDeviation;
	}

	get maxApparentSkill(): number {
		return this.rating + 2 * this.ratingDeviation;
	}

	get numMatches(): number {
		return this._numMatches;
	}

	get ratingHistory() {
		return this._ratingHistory;
	}

	set ratingElo(value: number) {
		this._ratingElo = value;
	}

	checkSkillConfiability(maxSkill: number, uncertaintyRate: number): boolean {
		let bottomDiff = this.skill - this.minApparentSkill;
		let superiorDiff = this.maxApparentSkill - this.skill;
		return (this.ratingDeviation <= maxSkill * uncertaintyRate &&
			bottomDiff <= this.skill * uncertaintyRate &&
			bottomDiff >= 0) ||
			(superiorDiff <= this.skill * uncertaintyRate && superiorDiff >= 0)
			? true
			: false;
	}

	addResult(opponent: Player, outcome: Outcome) {
		this._opponentRatings.push(opponent._rating);
		this._opponentRatingDeviations.push(opponent._ratingDeviation);
		this._outcomes.push(outcome);

		switch (outcome) {
			case Outcome.Loss: {
				this._loses++;
				break;
			}
			case Outcome.Tie: {
				this._ties++;
				break;
			}
			case Outcome.Win: {
				this._wins++;
				break;
			}
		}

		this._numMatches++;
	}

	// Calculates the new rating and rating deviation of the player.
	// Follows the steps of the algorithm described here:
	// http://www.glicko.net/glicko/glicko2.pdf
	updateRating() {
		if (!this.hasPlayed()) {
			// Applies only the Step 6 of the algorithm
			this.preRatingDeviation();
			return;
		}

		// Step 1: done by Player initialization
		// Step 2: done by set _rating and set _ratingDeviation

		// Step 3
		const v = this.variance();

		// Step 4
		const delta = this.delta(v);

		// Step 5
		this._volatility = this.volatilityAlgorithm(v, delta);

		// Step 6
		this.preRatingDeviation();

		// Step 7
		this._ratingDeviation =
			1 / Math.sqrt(1 / Math.pow(this._ratingDeviation, 2) + 1 / v);

		let tempSum = 0;
		for (let i = 0, len = this._opponentRatings.length; i < len; i++) {
			tempSum +=
				this.g(this._opponentRatingDeviations[i]) *
				(this._outcomes[i] -
					this.E(this._opponentRatings[i], this._opponentRatingDeviations[i]));
		}
		this._rating += Math.pow(this._ratingDeviation, 2) * tempSum;

		// Step 8: done by getters of `rating` and `ratingDeviation`

		// Cleanup
		this.cleanPreviousMatches();

		this._ratingHistory.push(this.rating);
	}

	private isCompatiblePlayer(player: Player) {
		return (
			player.tau === this.tau && player.defaultRating === this.defaultRating
		);
	}

	private cleanPreviousMatches() {
		this._opponentRatings = [];
		this._opponentRatingDeviations = [];
		this._outcomes = [];
	}

	private hasPlayed() {
		return this._outcomes.length > 0;
	}

	private volatilityAlgorithm(v: number, delta: number) {
		// Step 5.1
		let A = Math.log(Math.pow(this._volatility, 2));
		const f = this.fFactory(delta, v, A);
		const epsilon = 0.000001;

		// Step 5.2
		let B: number, k: number;
		if (Math.pow(delta, 2) > Math.pow(this._ratingDeviation, 2) + v) {
			B = Math.log(Math.pow(delta, 2) - Math.pow(this._ratingDeviation, 2) - v);
		} else {
			k = 1;
			while (f(A - k * this.tau) < 0) {
				k = k + 1;
			}
			B = A - k * this.tau;
		}

		// Step 5.3
		let fA = f(A);
		let fB = f(B);

		// Step 5.4
		let C: number, fC: number;
		while (Math.abs(B - A) > epsilon) {
			C = A + ((A - B) * fA) / (fB - fA);
			fC = f(C);
			if (fC * fB < 0) {
				A = B;
				fA = fB;
			} else {
				fA = fA / 2;
			}
			B = C;
			fB = fC;
		}
		// Step 5.5
		return Math.exp(A / 2);
	}

	// Calculates and updates the player's rating deviation for the beginning of a
	// rating period.
	private preRatingDeviation() {
		this._ratingDeviation = Math.sqrt(
			Math.pow(this._ratingDeviation, 2) + Math.pow(this._volatility, 2)
		);
	}

	// Calculation of the estimated variance of the player's rating based on game
	// _outcomes
	private variance() {
		let tempSum = 0;
		for (let i = 0, len = this._opponentRatings.length; i < len; i++) {
			const tempE = this.E(
				this._opponentRatings[i],
				this._opponentRatingDeviations[i]
			);
			tempSum +=
				Math.pow(this.g(this._opponentRatingDeviations[i]), 2) *
				tempE *
				(1 - tempE);
		}
		return 1 / tempSum;
	}

	// The Glicko E function.
	private E(p2rating: number, p2ratingDeviation: number) {
		return (
			1 /
			(1 + Math.exp(-1 * this.g(p2ratingDeviation) * (this._rating - p2rating)))
		);
	}

	// The Glicko2 g(ratingDeviation) function.
	private g(ratingDeviation: number) {
		return (
			1 /
			Math.sqrt(1 + (3 * Math.pow(ratingDeviation, 2)) / Math.pow(Math.PI, 2))
		);
	}

	// The delta function of the Glicko2 system.
	// Calculation of the estimated improvement in rating (step 4 of the algorithm)
	private delta(v: number) {
		let tempSum = 0;
		for (let i = 0, len = this._opponentRatings.length; i < len; i++) {
			tempSum +=
				this.g(this._opponentRatingDeviations[i]) *
				(this._outcomes[i] -
					this.E(this._opponentRatings[i], this._opponentRatingDeviations[i]));
		}
		return v * tempSum;
	}

	private fFactory(delta: number, v: number, a: number) {
		return (x: number) => {
			return (
				(Math.exp(x) *
					(Math.pow(delta, 2) -
						Math.pow(this._ratingDeviation, 2) -
						v -
						Math.exp(x))) /
					(2 *
						Math.pow(Math.pow(this._ratingDeviation, 2) + v + Math.exp(x), 2)) -
				(x - a) / Math.pow(this.tau, 2)
			);
		};
	}

	toObject() {
		return {
			opponentRatingDeviations: this.opponentRatingDeviations,
			opponentRatings: this.opponentRatings,
			outcomes: this.outcomes,
			rating: this.rating,
			skill: this.skill,
			ratingDeviation: this.ratingDeviation,
			volatility: this.volatility,
			matches: this._numMatches,
			victories: this._wins,
			defeats: this._loses,
		};
	}

	toJSON() {
		return this.toObject();
	}

	toString() {
		return `${this._name}: Rating: ${this.rating.toFixed(
			2
		)} | RD: ${this.ratingDeviation.toFixed(
			2
		)} | Real Skill: ${this.skill.toFixed(
			2
		)} | Apparent Skill: ${this.minApparentSkill.toFixed(
			2
		)}~${this.maxApparentSkill.toFixed(2)}`;
	}

	static createPlayerFactory(
		name: string,
		skill: number,
		{
			defaultRating = 1500,
			defaultRatingDeviation = 350,
			defaultVolatility = 0.06,
			tau = 0.5,
		},
		id: string = null
	): Player {
		const options = {
			opponentRatingDeviations: [],
			opponentRatings: [],
			outcomes: [],
			rating: defaultRating,
			ratingDeviation: defaultRatingDeviation,
			volatility: defaultVolatility,
		};

		return new Player(name, skill, {
			defaultRating,
			opponentRatingDeviations: options.opponentRatingDeviations,
			opponentRatings: options.opponentRatings,
			outcomes: options.outcomes,
			rating: options.rating,
			ratingDeviation: options.ratingDeviation,
			tau,
			volatility: options.volatility,
		});
	}

	toStringFormatted(ratingLength: number, rdLength: number) {
		let rating = Utils.normalizeStringLength(
			this.rating.toFixed(2),
			ratingLength
		);
		let rd = Utils.normalizeStringLength(
			this.ratingDeviation.toFixed(2),
			rdLength
		);
		let realSkill = Utils.normalizeStringLength(
			this.skill.toFixed(2),
			ratingLength
		);
		let minApparentSkill = Utils.normalizeStringLength(
			this.minApparentSkill.toFixed(2),
			ratingLength
		);
		let maxApparentSkill = Utils.normalizeStringLength(
			this.maxApparentSkill.toFixed(2),
			ratingLength
		);

		return `${this._name}: Rating: ${rating} | RD: ${rd} | Real Skill: ${realSkill} | Apparent Skill: ${minApparentSkill}~${maxApparentSkill} | M: ${this._numMatches} | WxL: ${this._wins} x ${this._loses}`;
	}
}
