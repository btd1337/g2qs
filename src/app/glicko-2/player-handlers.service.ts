import { Injectable } from '@angular/core';
import { ConstantsService } from './constants.service';
import Player from './player.entity';

@Injectable()
export class PlayerHandlersService {
	constructor(private constantsService: ConstantsService) {}

	public randomInt = (min: number, max: number): number => {
		return this.constantsService.RANDOM.integer({ min, max });
	};

	public random_normal(
		mean: number = this.constantsService.DEFAULT_RATING,
		dev: number = this.constantsService.DEFAULT_PLAYER_DEVIATION
	): number {
		let value = this.constantsService.RANDOM.normal({ mean: mean, dev: dev });
		return value >= this.constantsService.MIN_SKILL
			? value
			: this.constantsService.MIN_SKILL;
	}

	public check_normal_distribution(
		scale: number = this.constantsService.DEFAULT_RATING / 10,
		granularity: number = 20,
		numElements: number = 1000000
	): void {
		let values: number[] = new Array(granularity);
		values.fill(0);

		for (let i: number = 0; i < numElements; i++) {
			let index: number = Number((this.random_normal() / scale).toFixed());
			if (index >= 0 && index < granularity) {
				values[index] += 1;
			}
		}

		for (let i = 0; i < granularity; i++) {
			console.log(`${i * scale} - ${(i + 1) * scale}: ${values[i]}`);
		}
	}

	public print_players(
		players: Player[]
	): { maxMatches: string; mediumMatches: string; mediumRD: string } {
		let maxMatches: number = 0;
		let mediumRD: number = 0;
		let mediumMatches: number = 0;
		for (let i: number = 0; i < players.length; i++) {
			if (players[i].numMatches > maxMatches) {
				maxMatches = players[i].numMatches;
			}
			mediumRD += players[i].ratingDeviation;
			mediumMatches += players[i].numMatches;
			/* console.log(
				players[i].toStringFormatted(
					this.constantsService.RATING_LENGTH,
					this.constantsService.RD_LENGTH
				)
			); */
		}
		mediumRD = mediumRD / players.length;

		// Remove worst case
		// mediumMatches = mediumMatches - maxMatches;
		mediumMatches = mediumMatches / players.length;
		/* console.log(
			`\nMax Matches: ${maxMatches} | Medium Matches: ${mediumMatches.toFixed(
				2
			)} | Medium RD: ${mediumRD.toFixed(2)}`
		); */
		return {
			maxMatches: maxMatches.toString(),
			mediumMatches: mediumMatches.toFixed(2),
			mediumRD: mediumRD.toFixed(2),
		};
	}
}
