export interface ChartProperties {
	autoScale?: boolean;
	legend: boolean;
	legendTitle: string;
	showLabels: boolean;
	animations: boolean;
	xAxis: boolean;
	yAxis: boolean;
	showYAxisLabel: boolean;
	showXAxisLabel: boolean;
	xAxisLabel: string;
	yAxisLabel: string;
	timeline: boolean;
	view: any[];
	data: any[];
	colorScheme: {
		domain: string[];
	};
}
