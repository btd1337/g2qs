export interface ChartHorizontalBarData {
	name: string;
	value: number;
}
